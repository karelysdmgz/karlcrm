KarlCRM es un proyecto de web API desarrollada en dotNet Core 3.1
	
al implementar esta API tenemos que podemos realizar CRUD de Empresa, Productos, tipos de productos y contactos



# Las tecnologias involucradas tenemos #
* DotNet Core 3.1
* Entity Framework 3
* Microsoft Sql Server 16
* Swagger para probar cada metodo
* Datadog, para implementar metricas, en el proyecto no se implementan pero teiene esta capacidad
* NLog, se agrega esta caracateristica que es muy importante para auditoria



# Como ejecutar la aplicacion #
* Clonando el proyecto: `git clone https://gitlab.com/karelysdmgz/karlcrm.git`
* Con la IDE Visual Studio 2019, cargar la solucion del proyecto
* Compilar el proyecto, puediera ralizar un clear antes si fuera necesario
* ejecutar la api en el navegador de preferencia
* automaticamente muestra la interface de Swagger
* aqui podemos comenzar a aprobar cada metodo


# Algunas cosas que nos pueda suceder #
si nos genera un error al ejecutar, de path
* hacer click derecho en la aplicacion karlcrm-API, luego propiedades se despliega una ventana ir a la pestana Build y buscar la propiedad XML Documentation file



