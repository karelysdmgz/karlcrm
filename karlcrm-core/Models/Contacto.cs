﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace karlcrm_core.Models
{
    [Table("Contacto")]
    public class Contacto
    {
        [Column("Id_Contacto")]
        [Key]
        public int IDContacto { get; set; }

        [Column("Id_Empresa")]
        public int IDEmpresa { get; set; }

        [Column("Nombre")]
        public String Nombre { get; set; }

        [Column("Apellido")]
        public String Apellido { get; set; }

        [Column("Correo")]
        public string Correo { get; set; }

        [Column("Telefono")]
        public string Telefono { get; set; }

        [Column("Cargo")]
        public string Cargo { get; set; }
    }
}
