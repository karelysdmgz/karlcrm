﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace karlcrm_core.Models
{
    [Table("Tipo_Producto")]
    public class Tipo_Producto
    {
        [Column("ID_Tipo_Producto")]
        [Key]
        public int IDtipoProducto { get; set; }

        [Column("Nombre")]
        public String Nombre { get; set; }

        [Column("Descripcion")]
        public String Descripcion { get; set; }
    }
}
