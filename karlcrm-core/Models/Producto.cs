﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace karlcrm_core.Models
{
    [Table("Producto")]
    public class Producto
    {
        [Column("ID_Producto")]
        [Key]
        public int IDProducto{ get; set; }

        [Column("Nombre")]
        public String Nombre { get; set; }

        [Column("Descripcion")]
        public String Descripcion { get; set; }

        [Column("ID_Tipo_Producto")]
        public int IDTipoProducto { get; set; }

        [Column("Id_Empresa")]
        public int IDEmpresa { get; set; }


    }
}
