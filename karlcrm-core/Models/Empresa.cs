﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace karlcrm_core.Models
{
    [Table("Empresa")]
    public class Empresa
    {
        [Column("Id_Empresa")]
        [Key]
        public int IDEmpresa { get; set; }

        [Column("Nif")]
        public String Nif { get; set; }

        [Column("Razon_Social")]
        public String RazonSocial { get; set; }

        [Column("Direccion")]
        public string Direccion { get; set; }

        [Column("Fecha_Creado")]
        [DataType(DataType.DateTime)]
        public DateTime FechaCreado { get; set; }

        [Column("Status")]
        public bool Status { get; set; }

    }
}
