﻿using karlcrm_core.Models;
using karlcrm_data.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Data.SqlClient;

namespace karlcrm_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : ControllerBase
    {
        private readonly DbContextOptions<karlcrm_data.Data.DataContext> _dbOptions;

        public ProductoController(IServiceProvider serviceProvider)
        {
            _dbOptions = serviceProvider.GetService<DbContextOptions<karlcrm_data.Data.DataContext>>();
        }

        // GET: api/Productos
        /// <summary>
        /// Obtener Lista de Producto
        /// </summary>
        /// <returns>Cada Producto retorna: </returns>
        [HttpGet]
        public System.Collections.Generic.List<Producto> Get()
        {
            using (DataContext data = new DataContext(_dbOptions))
            {
                return data.Productos.FromSqlRaw("SELECT * from Producto").ToList();
            }

        }


        // GET: api/ProductoId
        /// <summary>
        /// Obtener Producto por Id
        /// </summary>
        /// <param name="id"> ID del Producto</param>
        /// <returns> </returns>
        [HttpGet("{id}", Name = "GetProducto")]
        public Producto Get(int id)
        {
            using (DataContext data = new DataContext(_dbOptions))
            {
                return data.Productos.Find(id);
            }
        }

        // POST: api/Producto Create
        /// <summary>
        /// Crear producto
        /// </summary>
        /// <param name="Nombre"> Nombre del producto></param>
        /// <param name="Descripcion">Descripción del producto</param>
        /// <param name="IDTipoProducto">Id del tipo de producto</param>
        /// <param name="IDEmpresa">Id de la empresa</param>
        /// <returns></returns>
        [HttpPost]
        public string Post(string Nombre, string Descripcion, int IDTipoProducto, int IDEmpresa)
        {
            string mensaje = string.Empty;

            Producto producto = new Producto();
            producto.Nombre = Nombre;
            producto.Descripcion = Descripcion;
            producto.IDTipoProducto = IDTipoProducto;
            producto.IDEmpresa = IDEmpresa;

            try
            {
                using (DataContext data = new DataContext(_dbOptions))
                {
                    data.Productos.Add(producto);
                    data.SaveChanges();
                }
                mensaje = "Ok";
            }
            catch (Exception ex)
            {
                mensaje = ex.InnerException.Message;
            }

            return mensaje;
        }

        // PUT: api/Producto Update
        /// <summary>
        /// Editar Producto
        /// </summary>
        /// <param name="producto"> Entidad de tipo Producto</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public string Put(int id, Producto producto)
        {
            string mensaje = string.Empty;
            producto.IDProducto = id;

            try
            {
                using (DataContext data = new DataContext(_dbOptions))
                {
                    data.Productos.Update(producto);
                    data.SaveChanges();
                }
                mensaje = "Ok";
            }
            catch (Exception ex)
            {
                mensaje = ex.InnerException.Message;
            }
            return mensaje;
        }

        // DELETE: api/Producto Delete
        /// <summary>
        /// Eliminar Producto
        /// </summary>
        /// <param name="id">Id del Producto</param>
        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            string mensaje = string.Empty;
            try
            {
                using (DataContext data = new DataContext(_dbOptions))
                {
                    Producto producto = data.Productos.Find(id);
                    data.Productos.Remove(producto);
                    data.SaveChanges();
                }
                mensaje = "Ok";
            }
            catch (Exception ex)
            {
                mensaje = ex.InnerException.Message;
            }
            return mensaje;

        }

        /// <summary>
        /// Obtener Producto por tipo de producto
        /// </summary>
        /// <param name="id_tipo_producto">Id del producto</param>
        /// <returns></returns>
        [HttpGet("GetByIdTipoProducto/{id_tipo_producto}", Name = "GetByIdTipoProducto")]
        public System.Collections.Generic.List<Producto> GetByIdTipoProducto(int id_tipo_producto)
        {
            using (DataContext data = new DataContext(_dbOptions))
            {
                var tipo = new SqlParameter("@id_tipo_producto", id_tipo_producto);
                return data.Productos.FromSqlRaw("SELECT * from Producto where id_tipo_producto= @id_tipo_producto", tipo).ToList();
            }

        }

        /// <summary>
        /// Obtener los producto de una empresa
        /// </summary>
        /// <param name="id_empresa">Id de la empresa</param>
        /// <returns></returns>
        [HttpGet("GetByIdEmpresa/{id_empresa}", Name = "GetByIdEmpresa")]
        public System.Collections.Generic.List<Producto> GetByIdEmpresa(int id_empresa)
        {
            using (DataContext data = new DataContext(_dbOptions))
            {
                var IdEmpresa = new SqlParameter("@id_empresa", id_empresa);
                return data.Productos.FromSqlRaw("SELECT * from Producto where id_empresa= @id_empresa", IdEmpresa).ToList();
            }

        }

    }
}
