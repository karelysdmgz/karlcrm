﻿using karlcrm_core.Models;
using karlcrm_data.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Data.SqlClient;

namespace karlcrm_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpresaController : ControllerBase
    {
        private readonly DbContextOptions<karlcrm_data.Data.DataContext> _dbOptions;

        public EmpresaController(IServiceProvider serviceProvider)
        {
            _dbOptions = serviceProvider.GetService<DbContextOptions<karlcrm_data.Data.DataContext>>();
        }

        // GET: api/Empresas
        /// <summary>
        /// Obtener Lista de Empresa
        /// </summary>
        /// <returns>Cada Empresa retorna: Id_Empresa,Nif, RazonSocial, Dirección, Fecha de Creado y status</returns>
        [HttpGet]
        public System.Collections.Generic.List<Empresa> Get()
        {
            using (DataContext data = new DataContext(_dbOptions))
            {
                return data.Empresas.FromSqlRaw("SELECT * from Empresa").ToList();
            }

        }


        // GET: api/EmpresaId
        /// <summary>
        /// Obtener Empresa por Id
        /// </summary>
        /// <param name="id"> ID de la empresa</param>
        /// <returns> </returns>
        [HttpGet("{id}", Name = "Get")]
        public Empresa Get(int id)
        {
            using (DataContext data = new DataContext(_dbOptions))
            {
                return data.Empresas.Find(id);
            }
        }

        // POST: api/Empresas Create
        /// <summary>
        /// Crear Empresa 
        /// </summary>
        /// <param name="nif"> Nif de la Empresa</param>
        /// <param name="razonSocial">Razon Social</param>
        /// <param name="direccion">Direccion </param>
        /// <param name="status">Status; inactiva false, activa true</param>
        [HttpPost]
        public string Post(string nif, string razonSocial, string direccion, bool status)
        {
            string mensaje = string.Empty;

            Empresa empresa = new Empresa();
            empresa.Nif = nif;
            empresa.RazonSocial = razonSocial;
            empresa.Direccion = direccion;
            empresa.FechaCreado = DateTime.Now;
            empresa.Status = status;

            try
            {
                using (DataContext data = new DataContext(_dbOptions))
                {
                    data.Empresas.Add(empresa);
                    data.SaveChanges();
                }
                mensaje = "Ok";
            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
            }

            return mensaje;
        }

        // PUT: api/Empresas Update
        /// <summary>
        /// Editar Empresa
        /// </summary>
        /// <param name="empresa"> Entidad de tipo empresa</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public string Put(int id, Empresa empresa)
        {
            string mensaje = string.Empty;
            empresa.IDEmpresa = id;

            try
            {
                using (DataContext data = new DataContext(_dbOptions))
                {
                    data.Empresas.Update(empresa);
                    data.SaveChanges();
                }
                mensaje = "Ok";
            }
            catch (Exception ex)
            {
                mensaje = ex.InnerException.Message;
            }
            return mensaje;
        }

        // DELETE: api/Empresa Delete
        /// <summary>
        /// Eliminar Empresa
        /// </summary>
        /// <param name="id">Id de la empresa</param>
        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            string mensaje = string.Empty;
            try
            {
                using (DataContext data = new DataContext(_dbOptions))
                {
                    Empresa empresa = data.Empresas.Find(id);
                    var IdEmpresa = new SqlParameter("@id_empresa", id);
                    List<Contacto> contactos = data.Contactos.FromSqlRaw("SELECT * from Contacto where id_empresa= @id_empresa", IdEmpresa).ToList();

                    foreach (Contacto contacto in contactos)
                    {
                        data.Contactos.Remove(contacto);
                        data.SaveChanges();
                    }

                    List<Producto> productos = data.Productos.FromSqlRaw("SELECT * from Producto where id_empresa= @id_empresa", IdEmpresa).ToList();

                    foreach (Producto producto in productos)
                    {
                        data.Productos.Remove(producto);
                        data.SaveChanges();
                    }

                    data.Empresas.Remove(empresa);
                    data.SaveChanges();
                }
                mensaje = "Ok";
            }
            catch (Exception ex)
            {
                mensaje = ex.InnerException.Message;
            }
            return mensaje;
           
        }

        /// <summary>
        /// Obtener lista de empresas por status
        /// </summary>
        /// <param name="status"> Status de la Empresa</param>
        /// <returns></returns>
        [HttpGet("GetByStatus/{status}", Name = "GetByStatus")]
        public System.Collections.Generic.List<Empresa> GetByStatus(bool status)
        {
            using (DataContext data = new DataContext(_dbOptions))
            {
                var estatus = new SqlParameter("@status", status);
                return data.Empresas.FromSqlRaw("SELECT * from Empresa where status= @status", estatus).ToList();
            }

        }
               
    }
}
