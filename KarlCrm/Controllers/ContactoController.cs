﻿using karlcrm_core.Models;
using karlcrm_data.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Data.SqlClient;

namespace karlcrm_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactoController : ControllerBase
    {
        private readonly DbContextOptions<karlcrm_data.Data.DataContext> _dbOptions;

        public ContactoController(IServiceProvider serviceProvider)
        {
            _dbOptions = serviceProvider.GetService<DbContextOptions<karlcrm_data.Data.DataContext>>();
        }

        // GET: api/Contactos
        /// <summary>
        /// Obtener Lista de Contactos
        /// </summary>
        /// <returns>Cada Contacto retorna: </returns>
        [HttpGet]
        public System.Collections.Generic.List<Contacto> Get()
        {
            using (DataContext data = new DataContext(_dbOptions))
            {
                return data.Contactos.FromSqlRaw("SELECT * from Contacto").ToList();
            }

        }


        // GET: api/ContactoId
        /// <summary>
        /// Obtener Contacto por Id
        /// </summary>
        /// <param name="id"> ID del Contacto</param>
        /// <returns> </returns>
        [HttpGet("{id}", Name = "GetContacto")]
        public Contacto Get(int id)
        {
            using (DataContext data = new DataContext(_dbOptions))
            {
                return data.Contactos.Find(id);
            }
        }

        // POST: api/Contacto Create
        /// <summary>
        /// Crear contacto de la empresa
        /// </summary>
        /// <param name="IDEmpresa">Id de la empresa</param>
        /// <param name="Nombre">Nombre</param>
        /// <param name="Apellido">Apellido</param>
        /// <param name="Correo">Correo</param>
        /// <param name="Telefono">Telefono</param>
        /// <param name="Cargo">Cargo</param>
        /// <returns></returns>
        [HttpPost]
        public string Post(int IDEmpresa, string Nombre, string Apellido, string Correo, string Telefono, string Cargo)
        {
            string mensaje = string.Empty;

            Contacto contacto = new Contacto();
            contacto.IDEmpresa = IDEmpresa;
            contacto.Nombre = Nombre;
            contacto.Apellido = Apellido;
            contacto.Correo = Correo;
            contacto.Telefono = Telefono;
            contacto.Cargo = Cargo;

            try
            {
                using (DataContext data = new DataContext(_dbOptions))
                {
                    data.Contactos.Add(contacto);
                    data.SaveChanges();
                }
                mensaje = "Ok";
            }
            catch (Exception ex)
            {
                mensaje = ex.InnerException.Message;
            }

            return mensaje;
        }

        // PUT: api/Contacto Update
        /// <summary>
        /// Editar Contacto de la empresa
        /// </summary>
        /// <param name="id"></param>
        /// <param name="contacto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public string Put(int id, Contacto contacto)
        {
            string mensaje = string.Empty;
            contacto.IDContacto = id;

            try
            {
                using (DataContext data = new DataContext(_dbOptions))
                {
                    data.Contactos.Update(contacto);
                    data.SaveChanges();
                }
                mensaje = "Ok";
            }
            catch (Exception ex)
            {
                mensaje = ex.InnerException.Message;
            }
            return mensaje;
        }

        // DELETE: api/Contacto Delete
        /// <summary>
        /// Eliminar Contacto de la empresa
        /// </summary>
        /// <param name="id">Id del contacto</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            string mensaje = string.Empty;
            try
            {
                using (DataContext data = new DataContext(_dbOptions))
                {
                    Contacto contacto = data.Contactos.Find(id);
                    data.Contactos.Remove(contacto);
                    data.SaveChanges();
                }
                mensaje = "Ok";
            }
            catch (Exception ex)
            {
                mensaje = ex.InnerException.Message;
            }
            return mensaje;

        }

        /// <summary>
        /// Obtener lista de contactos por ID de la empresa
        /// </summary>
        /// <param name="id_empresa"></param>
        /// <returns></returns>
        [HttpGet("GetByEmpresaId/{id_empresa}", Name = "GetByEmpresaId")]
        public System.Collections.Generic.List<Contacto> GetByEmpresaId(int id_empresa)
        {
            using (DataContext data = new DataContext(_dbOptions))
            {
                var IdEmpresa = new SqlParameter("@id_empresa", id_empresa);
                return data.Contactos.FromSqlRaw("SELECT * from Contacto where id_empresa= @id_empresa", IdEmpresa).ToList();
            }

        }
    }
}
