﻿using karlcrm_core.Models;
using karlcrm_data.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Data.SqlClient;

namespace karlcrm_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Tipo_ProductoController : ControllerBase
    {
        private readonly DbContextOptions<karlcrm_data.Data.DataContext> _dbOptions;

        public Tipo_ProductoController(IServiceProvider serviceProvider)
        {
            _dbOptions = serviceProvider.GetService<DbContextOptions<karlcrm_data.Data.DataContext>>();
        }

        // GET: api/Tipo_Producto
        /// <summary>
        /// Obtener Lista de Tipo de Producto
        /// </summary>
        /// <returns>Cada Tipo de producto retorna: </returns>
        [HttpGet]
        public System.Collections.Generic.List<Tipo_Producto> Get()
        {
            using (DataContext data = new DataContext(_dbOptions))
            {
                return data.Tipos_Productos.FromSqlRaw("SELECT * from Tipo_Producto").ToList();
            }

        }


        // GET: api/Tipo_ProductoId
        /// <summary>
        /// Obtener Tipo de producto por Id
        /// </summary>
        /// <param name="id"> ID del tipo de producto</param>
        /// <returns> </returns>
        [HttpGet("{id}", Name = "GetTipoProducto")]
        public Tipo_Producto Get(int id)
        {
            using (DataContext data = new DataContext(_dbOptions))
            {
                return data.Tipos_Productos.Find(id);
            }
        }

        // POST: api/Tipo_Producto Create
        /// <summary>
        /// Crear Tipo de Producto
        /// </summary>
        /// <param name="Nombre">Nombre del producto</param>
        /// <param name="Descripcion">Descripcion del producto</param>
        /// <returns></returns>
        [HttpPost]
        public string Post(string Nombre, string Descripcion)
        {
            string mensaje = string.Empty;

            Tipo_Producto tipo = new Tipo_Producto();
            tipo.Nombre = Nombre;
            tipo.Descripcion = Descripcion;

            try
            {
                using (DataContext data = new DataContext(_dbOptions))
                {
                    data.Tipos_Productos.Add(tipo);
                    data.SaveChanges();
                }
                mensaje = "Ok";
            }
            catch (Exception ex)
            {
                mensaje = ex.InnerException.Message;
            }

            return mensaje;
        }

        // PUT: api/Tipo_Producto Update
        /// <summary>
        /// Editar tipo de producto
        /// </summary>
        /// <param name="tipo"> Entidad de tipo de producto</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public string Put(int id, Tipo_Producto tipo)
        {
            string mensaje = string.Empty;
            tipo.IDtipoProducto = id;

            try
            {
                using (DataContext data = new DataContext(_dbOptions))
                {
                    data.Tipos_Productos.Update(tipo);
                    data.SaveChanges();
                }
                mensaje = "Ok";
            }
            catch (Exception ex)
            {
                mensaje = ex.InnerException.Message;
            }
            return mensaje;
        }

        // DELETE: api/Tipo_Producto Delete
        /// <summary>
        /// Eliminar Tipo de Producto
        /// </summary>
        /// <param name="id">Id del tipo de producto</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            string mensaje = string.Empty;
            try
            {
                using (DataContext data = new DataContext(_dbOptions))
                {
                    Tipo_Producto tipo = data.Tipos_Productos.Find(id);
                    data.Tipos_Productos.Remove(tipo);
                    data.SaveChanges();
                }
                mensaje = "Ok";
            }
            catch (Exception ex)
            {
                mensaje = ex.InnerException.Message;
            }
            return mensaje;

        }
    }
}
