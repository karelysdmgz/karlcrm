﻿using karlcrm_core.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;

namespace karlcrm_data.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { 
        }
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Contacto> Contactos { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<Tipo_Producto> Tipos_Productos { get; set; }
    }
}
